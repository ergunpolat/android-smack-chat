package uk.co.polat.ergun.smackchat.utils

/**
 * Created by Ergun Polat on 28/03/2018.
 */

const val BASE_URL = "https://chattychatjba.herokuapp.com/v1/"
const val URL_REGISTER = "${BASE_URL}account/register"