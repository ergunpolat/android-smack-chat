package uk.co.polat.ergun.smackchat.controller

import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import kotlinx.android.synthetic.main.activity_create_user.*
import uk.co.polat.ergun.smackchat.R
import uk.co.polat.ergun.smackchat.services.AuthService
import java.util.*

class CreateUserActivity : AppCompatActivity() {

    var userAvatar = "profileDefault"
    var avatarColour = "[0.5, 0.5, 0.5, 1]"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_user)
    }


    fun generateUserAvatar(view: View) {
        val random = Random()
        val colour = random.nextInt(2)
        val avatar = random.nextInt(28)

        if (colour == 0) {
            userAvatar = "light$avatar"
        } else {
            userAvatar = "dark$avatar"
        }

        val resourceId = resources.getIdentifier(userAvatar, "drawable", packageName)
        createAvatarImageView.setImageResource(resourceId)

    }

    fun generateColourClicked(view: View) {
        val random = Random()
        val r = random.nextInt(255)
        val g = random.nextInt(255)
        val b = random.nextInt(255)

        createAvatarImageView.setBackgroundColor(Color.rgb(r,g,b))

        val savedR = r.toDouble() / 255
        val savedG = g.toDouble() / 255
        val savedB = b.toDouble() / 255

        avatarColour = "[$savedR, $savedG, $savedB, 1]"

        println(avatarColour)

    }

    fun createUserClicked(view: View) {


        AuthService.registerUser(this,"b@b.com","123456") {complete ->
            if (complete) {
                Log.d("A", "completed")
            } else {
                Log.d("A", "not completed")
            }

        }

    }
}
